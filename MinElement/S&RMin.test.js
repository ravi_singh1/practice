import {minNum} from './S&RMin'

describe("Minimum Element Testing",()=>{
    test("Testing",()=>{
        expect(minNum([5,6,1,2,3,4],0)).toEqual(1);
    })
    test("Testing",()=>{
        expect(minNum([1,2],0)).toEqual(1);
    })
})