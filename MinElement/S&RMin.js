//Find minimum element in sorted rotated array. The time complexity should be O (logn).

export const minNum = (arr,low) =>{
    // High variable containing the last element of array
    let high=arr.length-1;
    while(low<high){
        //find mid
        let mid = Math.floor(low + (high-low)/2);
        if(arr[mid] == arr[high]) high--;
        else if(arr[mid] > arr[high]) low = mid + 1;
        else high = mid;
    }
    return arr[high];
}