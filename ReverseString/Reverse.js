//Resolve the issue of program reverse String test.
//(we need to check and remove first if condition inside reversed method).


export const reverse = (str) =>{
    let arr1='';
    //Making array of string variable (str)
    let arr = str.split("");
    //Reverse traversing
    for(let i=arr.length-1;i>=0;i--){
        //adding from last to first element of array into a variable
        arr1+=arr[i];
    }
    return arr1;
}