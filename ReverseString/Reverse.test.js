import {reverse} from './Reverse'

describe("Reversing the string",()=>{
    test("Testing",()=>{
        expect(reverse("hello")).toEqual("olleh");
    })
    test("Testing",()=>{
        expect(reverse("World")).toEqual("dlroW");
    })
})