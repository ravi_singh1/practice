export const pivot = (arr) => {
    if(arr.length == 1) return 0;
    let sumL = 0;
    let sumR = 0;
    for(let i=0;i<arr.length;i++){
        for(let j=0;j<=i;j++){
            sumL += arr[j];
        }
        for(let m=i;m<arr.length;m++){
            sumR += arr[m];
        }
        if(sumL == sumR && sumL!= 0 && sumR!=0) return i;
        sumL=0;
        sumR=0;
    }
}