//Find the in order traversal method

//creation of Node object
function Node(data,left,right){
    this.data=data;
    this.left=left;
    this.right=right;

    //Display the data
    this.show=()=> this.data;
}

//Binary Search Tree class
function BST(){
    //root of binary search tree
    this.root = null;

    //it inserts a new node in a tree with a value data
    this.insert=(data)=>{

        //Creating a node and initializing with data
        let n = new Node(data,null,null);

        //root is null then node will be added to the tree and made root.
        if(this.root == null) {
            this.root = n;
        }else {
            //Saving root data into current new variable
            let current = this.root;
            let parent;
            while(true){
                parent = current;
                //if the data is less than the node data move left of the tree
                if(data < current.data){
                    current = current.left;
                    //if left is null insert node here
                    if(current == null){
                        parent.left = n;
                        break;
                    }
                }else{
                    current = current.right;
                    //if right is null insert node here and break
                    if(current == null){
                        parent.right = n;
                        break;
                    }
                }
            }
        }
    }
}
//Performs inorder traversal of a tree
function inOrder(node){
    if(!(node == null)){
        inOrder(node.left);
        //Pushing data into an array
        batch.push(node.show());
        inOrder(node.right);
    }
}
//Making new Object and save into the nums 
let nums = new BST();
export const Order = (str) => {
    for(let i =0; i < str.length;i++){
        nums.insert(str[i]);
    }
    inOrder(nums.root);
    return batch;
}
const arr = [23,45,16,37,3,99,22];
const batch = [];

