import {Order} from './InOrder'
describe('Inorder Traversing',()=>{
    test('Inorder Traversing',()=>{
        expect(Order([23,45,16,37,3,99,22])).toEqual[3,16,22,23,37,45,99];
    })
    test('Inorder Traversing',()=>{
        expect(Order([2,5,3,7,4,8,6,9,1])).toEqual[1,2,3,4,5,6,7,8,9];
    })
})