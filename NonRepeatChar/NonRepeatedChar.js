//Non repeating character in string
export const nonRepeat = (str) =>{
    // Making array from string
    let arr = str.split('');
    let result = '';
    let ctr = 0;
    //Iterate through array 
    for(let x = 0; x < arr.length; x++ ){
        ctr = 0;
        
        for(let y=0; y < arr.length; y++){
            //Comparison previous value and current value
            //if true increment ctr by 1
            if(arr[x] === arr[y]){
                ctr += 1;
            }
        }
        //if ctr is less than 2 then save the current value into a result variable and break
        if(ctr < 2){
            result = arr[x];
            break;
        }
    }
    return result;
}