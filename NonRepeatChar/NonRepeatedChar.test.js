import {nonRepeat} from './NonRepeatedChar';
describe('Non rpeated Character in string',()=>{
    test('String Test',()=>{
        expect(nonRepeat('abacddbec')).toEqual('e');
    })
    test('String Test',()=>{
        expect(nonRepeat('abccddbece')).toEqual('a');
    })
})