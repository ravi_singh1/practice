

import {cord} from './LongestCord'
describe('Longest Cord', ()=>{
    test('Test 1',()=>{
        expect(cord(["ale", "apple", "monkey", "plea"],'abpcplea')).toEqual('apple')
    });
    test('Test 2', ()=>{
        expect(cord(["info","inform","information"],'icndfaoarm')).toEqual('inform');
    })
})