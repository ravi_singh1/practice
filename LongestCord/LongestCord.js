//  Given a string as letters (“ODG”) find the longest cord which 
//matches this cord in a dictionary given (full of string words).
// Returns true if str1[] is a subsequence of str2[].
// m is length of str1 and n is length of str2    
function isSequel(str1, str2){
        let m = str1.length, n = str2.length;
        let j=0; // For index of str1 (or subsequence


    // Traverse str2 and str1, and compare current
    // character of str2 with first unmatched char
    // of str1, if matched then move ahead in str1
        for(let i = 0; i<n && j<m;i++){
            if(str1[j] == str2[i]){
            j++;
            }
        }
        // If all characters of str1 were found in str2    
        return (j == m);        
    }

// Returns the longest string in dictionary which is a
// subsequence of str.
    export const cord = (dict, str)=>{
        let str3 = str.toLowerCase();
        let result = '';
        let length = 0;
// Traverse through all words of dictionary        
        dict.forEach(word => {
        
        // If current word is subsequence of str and is
        // largest such word so far.            
            if(length < word.length && isSequel(word, str3)){
                result = word;
                length = word.length;
            }
        });
        // Return longest string
        return result;
    }
