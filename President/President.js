export const electPres =(n,k)=>{
    if(n==1) return 1;
    else {
        // Here by useing recurvise call of electPres giving position of next element
        // and also removing the previous element;
        return (electPres(n - 1,k) + k-1)%n+1;
    }
}