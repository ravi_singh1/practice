import {smallestNum} from './SmallestNum'

describe("Smallest Number in an Array",()=>{
    test("Testing Smallest Number",()=>{
        expect(smallestNum([5,1,9,5,7])).toEqual(1);
    })
    test("Testing Smallest Number",()=>{
        expect(smallestNum([12,67,32,56])).toEqual(12);
    })
})