export const smallestNum =(arr)=>{
    let num = arr[0];
    for(let i=1; i<arr.length;i++){
        if(num>arr[i]){
            num = arr[i];
        }
    }
    return num;
}