//Reverse Vowels in a String. 
//this function checks for vowel
const isVowel = (c) =>{
    return (c == 'a'||c == 'A'||c=='e'||c=='E'||c=='o'||c=='O'||c=='i'||c=='I'||c=='u'||c=='U');
}
export const revVowel = (str1) => {
    let j = 0;
    //Storing the vowels separatly
    let str = str1.split('');
    let vowel = "";
    for (let i=0;i<str.length;i++){
        if(isVowel(str[i])){
            j++;
            vowel +=str[i];
        }
    }

//Placing the vowels in the reverse order in the string.
    for(let i=0;i<str.length;i++){
        if(isVowel(str[i])){
            str[i] = vowel[--j];
        }
    }
    return str.join("");
}
