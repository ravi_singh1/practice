import { revVowel } from "./RevVowel";

describe("Testing for reverse vowel in given string",()=>{
    test("Testing",()=>{
        expect(revVowel("hello")).toEqual("holle");
    })
    test("Testing",()=>{
        expect(revVowel("hello World")).toEqual("hollo Werld");
    })
})