import {smSubArr} from './SmallestSubArr'

describe("Testing for smallest subarray",()=>{
    test("Testing",()=>{
        expect(smSubArr([2,4,6,10,2,1],12)).toEqual(2);
    })
})