//Implement a function that takes in an array of non-negative numbers and an integer. 
//You want to return the "LENGTH" of the shortest subarray whose sum is at least the integer 
//and -1 if no such sum exists.
export const smSubArr = (arr,k)=>{
    let n = arr.length;

    //Stores the frequency of prefix sums in the array
    let mp = new Map();

    mp.set(arr[0],0);
    for(let i =1;i<n;i++){
        arr[i] = arr[i] + arr[i-1];
        mp.set(arr[i],i);
    }

    //initialize len as INT_MAX
    var len = 10000000000;
    for(let i=0;i<n;i++){
        
        //if sum of array till i-th index is less than k
        if(arr[i] < k) continue;
        else {
           // find the exceeded value
           let x = arr[i] - k;
           //if exceeded value is zero
           if( x == 0) len = Math.min(len,i);
           if(!mp.has(x)) continue;
           else {
               len = Math.min(len,i-mp.get(x));
           }     
        }
    }
    return len;
}