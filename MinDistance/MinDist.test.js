import {minDist} from './MinDist'

describe("Minimum distance b/w mid element",()=>{
    test("Test",()=>{
        expect(minDist("quick","frog","the quick brown quick brown the frog")).toEqual(2);
    })
    test("Test",()=>{
        expect(minDist("cat","was","was it a car or a cat i saw")).toEqual(5);
    })
})