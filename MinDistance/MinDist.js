//Calculate the minimum distance between w1 and w2 in str
export const minDist = (w1,w2,str) =>{
    if(w1 == w2){
        return 0;
    }
    //get individual words in a list
    let words = str.split(" ");

    //assume total length of the string as minimum distance
    let min_dis = (words.length) +1;

    //traverse through the entire string
    for(let i=0; i<words.length; i++){
        if(words[i] == w1){
            for (let s=0;s<words.length;s++){
                if(words[s] == w2){
                    //the distance between the words is the i of the first word
                    //the current current word index
                    let curr = Math.abs(i-s) -1;

                    //Comparing current distance with the previously assumed distance
                    if(curr < min_dis){
                        min_dis = curr;
                    }
                }
            }
        }
    }
    return min_dis;
}