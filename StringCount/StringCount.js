//Convert this string (aabbb into a2b3) ,  (aaaa into a4), (a into a1).
export const StringCount = (str) => {
    // Create an empty object.
    let obj={};
    //Accessing the element of str argument.
    for(let i of str){
        //checking if obj is containing one character then its values is 1 and 
        //if more than one its value increment by 1.
        obj[i]?obj[i]++:obj[i]=1;
    }
    //Creating a variable to take values of saved obj variable and converting into an array
    let run = Object.entries(obj);
    //replaceing the comma and colon to make this arry into string.
    return run.join().replace(/,/g,'');
}