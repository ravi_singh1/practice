import {StringCount} from './StringCount';

describe('String Count',() => {
    test('Number of String Duplicate',()=>{
        expect(StringCount('aabb')).toEqual('a2b2');
    }),
    test('Number of String Count test 2',()=>{
        expect(StringCount('aaaa')).toEqual('a4');
    })
})