import {myAtoi} from './Atoi'

describe("Atoi",()=>{
    test("Testing",()=>{
        expect(myAtoi("-123")).toEqual(-123);
    })
    test("Testing",()=>{
        expect(myAtoi("@456")).toEqual(0);
    })
})