//Given a string which we have to convert into integer. 
//The string can be in form of either positive, negative, 
//Alphabetical or special characters. – (atoi function)

export const myAtoi = (str) =>{
 let sign =1, base = 0,i=0;
 //if whitespaces then ignore
 while(str[i]==' '){
     i++;
 }
 //sign of number
 if(str[i]=='-' || str[i]=='+'){
     sign = 1-2*(str[i++]=='-');
 }
 //checking for valid input
 while(str[i]>='0' && str[i]<='9'){
     if(base > Number.MAX_VALUE/10 || (base== Number.MAX_VALUE/10) && str[i]-'0'>7){
         if(sign == 1) return Number.MAX_VALUE
         else return Number.MAX_VALUE;
     }
     base = 10*base +(str[i++]-'0');
 }
 return base * sign;
}