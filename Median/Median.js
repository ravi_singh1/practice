//'Median Two Sorted Arrays' snippet
//Passing two arguments
export const median =(a,b)=>{
    //Sort the array
    let c = [...a, ...b].sort((a,b)=> a-b);
    //Get the floor value
    const half = c.length/2 | 0;
   //if odd then return middle element
    if(c.length%2){
       return c[half];
   }
   //If even then return the average of two mid elements
   else{
       return (c[half]+c[half-1])/2;
   }
}