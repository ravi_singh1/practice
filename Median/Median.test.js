import {median} from './Median';

describe('Median of Array',()=>{
    test("Median Testing",()=>{
        expect(median([1,2,3,6],[4,6,8,10])).toEqual(5);
    })
    test("Median Testing",()=>{
        expect(median([1,12,15,26,38],[2,13,17,30,45])).toEqual(16);
    })
    test("Median Testing",()=>{
        expect(median([1,12,15,26,38],[2,13,17,30,45,47])).toEqual(17);
    })
    test("Median Testing",()=>{
        expect(median([1,2],[3])).toEqual(2);
    })
})