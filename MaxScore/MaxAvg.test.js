import {maxAvg} from './MaxAvg'
describe("Maximum average testing",()=>{
    test("Testing",()=>{
        expect(maxAvg(["Shrikanth","20","30","10","Ram","100","50","10"])).toEqual("Ram 53");
    })
    test("Testing",()=>{
        expect(maxAvg(["Shrikanth","90","80","10","Ram","100","50","10"])).toEqual("Shrikanth 60");
    })
})