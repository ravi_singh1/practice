//Find maximum average from a list in which the marks obtained of a student 
//can be positive or negative and a student may have multiple marks. 

export const maxAvg = (f)=>{
    let n = f.length;
    //Variable to store average score of a student and maximim average score
    let avgScore;
    let maxAvgScore = Number.MIN_SAFE_INTEGER;

    //List to store names of students having maximum average score
    let name = [];

    //Traverse the file(f) data
    for(let i=0;i<n;i++){
        //finding average score of a student
        avgScore = Math.floor((Number(f[i+1])+Number(f[i+2])+Number(f[i+3]))/3);
        if(avgScore > maxAvgScore){
            maxAvgScore = avgScore;

            //Clear the list and add name of student having current maximum average score in the list
            name = [];
            name.push(f[i]);
        }
        else if( avgScore == maxAvgScore){
            name.push(f[i]);
        }
    }
    return name[0]+" "+ maxAvgScore;
}