//find the root with a minimum id of the largest tree in a forest.

let root;
class Node {
    constructor(data){
    this.left = null;
    this.right = null;
    this.data = data;
    }
}

export const minRoot = (node) => {
    if(node == null) return 2147483647;
    let res = node.data;
    let lres = minRoot(node.left);
    let rres = minRoot(node.right);
    if(lres < res) res = lres;
    if(rres < res) res = rres;
    return res;
}
root = new Node(2);
root.left = new Node(7);
root.right = new Node(5);
root.left.right = new Node(6);
root.left.right.left = new Node(1);
root.left.right.right = new Node(11);
root.right.right = new Node(9);
root.right.right.left = new Node(4);
console.log("Min element is " + minRoot(root));