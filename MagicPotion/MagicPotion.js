export const potion=(k,height)=>{
    // heightSort will hold our sorted height
    // and with sort method along with a helper function to sort the array
    let heightSort = height.sort((a,b)=> a-b);

    // if statement to compare the highest number in the array to our maximum
    // natural jump height,k;
    if(heightSort[heightSort.length -1]>k){
        // Here we are returning the maximum potion want to jump maximum height
        return heightSort[heightSort.length-1] -k;
    }else{
        return 0;
    }
}