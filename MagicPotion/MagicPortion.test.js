import {potion} from './MagicPotion'
describe("Magic Potion",()=>{
    test("Potion",()=>{
        expect(potion(2,[1,2,2,3])).toEqual(1);
    })
    test("Potion",()=>{
        expect(potion(2,[1,2,2,3,4])).toEqual(2);
    })
})